package apresentacao;


import java.util.Random;

import service.FabricaCondado;
import service.FabricaMordor;
import service.FabricaReino;
import service.Fugindo;
import service.Orc;
import service.Soco;
import entidades.Reino;







public class Main {

	
	static Orc orc = new Orc();
	static int total[] = {0,0};

	public static void main(String[] args) {

		FabricaReino fabrica;
		Random r = new Random();		
		
		
		
		System.out.println("Reinos");
		
		fabrica = new FabricaCondado();		
		Reino condado = montarReino(fabrica);
		mostraReino(condado);		
		
		fabrica = new FabricaMordor();
		Reino mordor = montarReino(fabrica);		
		mostraReino(mordor);		
	
		
		int lifeCastelo = condado.getCastelo().getDefesa();
		
		
		ataqueOrc(lifeCastelo,r,mordor);
		
		
		
	
		
	}
	
private static void mostraReino(Reino reino) {
	
	
	
	System.out.println("\n###"+reino.getTitulo().getDescricao()+"###");
	System.out.println("-----Castelo-----");
	
	
	System.out.println("Ataque-->"+reino.getCastelo().getAtaque());
	System.out.println("Defesa-->"+reino.getCastelo().getDefesa());
	System.out.println("-----Rei-----");
	System.out.println(reino.getRei().getNome());
	System.out.println("-----Exercito-----");
	System.out.println(reino.getExercito().getDescricao());
	System.out.println("Quantidade--> "+reino.getExercito().getQuantidade());
	System.out.println("Poder ataque/s--> "+reino.getExercito().getAtaquepower());
		
	}

private static void ataqueOrc(int lifeCastelo, Random r, Reino mordor) {
		
		System.out.println("\n#####Ataque dos Orcs#####");
		getstatus(lifeCastelo);
		
		do {
			int div = r.nextInt();
			total[0]++;
			
			if (div % 2 ==0 ){
				total[1]++;
				orc.acao(new Soco());
				lifeCastelo -= mordor.getExercito().getAtaquepower();
				getstatus(lifeCastelo);
			}else{
				total[0]++;
				
				orc.acao(new Fugindo());
				getstatus(lifeCastelo);
			}
			
		}while (lifeCastelo>=0 && total[0] <=mordor.getExercito().getQuantidade());
			
			
			exibeResultado(lifeCastelo);
		
	}

private static void exibeResultado(int lifeCastelo) {
	
	System.out.println("\n\n####Resultado####");
	
	if (lifeCastelo>0) {
		System.out.println("Orcs foram Derrotados");
		
	}else {
		System.out.println("Orcs Venceram");
	}
	
	System.out.println("Total de Orcs: "+total[0]);
	System.out.println("Atacaram: "+total[1]);
	System.out.println("Fugiram: "+(total[0]-total[1]));
	
}

private static void getstatus(int lifeCastelo) {
	if (lifeCastelo<=0){
		System.out.println("Castelo Destruido!!!");
	}else if (lifeCastelo>=0) {
		System.out.println("Life: "+lifeCastelo);	
	}	
	
		
	}



	
	

private static Reino montarReino(FabricaReino fabrica) {
		
		fabrica.getInstance();
		
	
		Reino reino = new Reino();		
		reino.setCastelo(fabrica.criarCastelo());
		reino.setRei(fabrica.criarRei());
		reino.setExercito(fabrica.criarExercito());

		reino.setTitulo(fabrica.criarTitulo());
		return reino;
		
		


	}
		
		
		
		

	
	}


	
	


