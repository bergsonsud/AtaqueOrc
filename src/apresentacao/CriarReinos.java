package apresentacao;

import service.FabricaCondado;
import service.FabricaMordor;
import service.FabricaReino;
import entidades.Reino;

public class CriarReinos {
	public CriarReinos() {
		
		FabricaReino fabrica = new FabricaCondado();		
		Reino condado = montarReino(fabrica);
		
		
		System.out.println("Reinos");
		System.out.println("\n#####Condado#####");
		System.out.println("-----Castelo-----");
		
		
		System.out.println("Ataque-->"+condado.getCastelo().getAtaque());
		System.out.println("Defesa-->"+condado.getCastelo().getDefesa());
		System.out.println("-----Rei-----");
		System.out.println(condado.getRei().getNome());
		System.out.println("-----Exercito-----");
		System.out.println(condado.getExercito().getDescricao());
		System.out.println("Quantidade--> "+condado.getExercito().getQuantidade());
		System.out.println("Poder ataque/s--> "+condado.getExercito().getAtaquepower());
		
		fabrica = new FabricaMordor();
		Reino mordor = montarReino(fabrica);
		
		
		System.out.println("\n#####Mordor#####");
		System.out.println("-----Castelo-----");
		
		
		System.out.println("Ataque-->"+mordor.getCastelo().getAtaque());
		System.out.println("Defesa-->"+mordor.getCastelo().getDefesa());
		System.out.println("-----Rei-----");
		System.out.println(mordor.getRei().getNome());
		System.out.println("-----Exercito-----");
		System.out.println(mordor.getExercito().getDescricao());
		System.out.println("Quantidade--> "+mordor.getExercito().getQuantidade());
		System.out.println("Poder ataque/s--> "+mordor.getExercito().getAtaquepower());
		
	
		
	}
	
private static Reino montarReino(FabricaReino fabrica) {
		
		
		Reino reino = new Reino();		
		reino.setCastelo(fabrica.criarCastelo());
		reino.setRei(fabrica.criarRei());
		reino.setExercito(fabrica.criarExercito());
		return reino;
		
		
		

	}

}
