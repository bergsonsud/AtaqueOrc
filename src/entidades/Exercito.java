package entidades;


public class Exercito  {
	
	private String descricao;
	private int quantidade;
	private int ataquepower;
	
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public int getAtaquepower() {
		return ataquepower;
	}
	public void setAtaquepower(int ataquepower) {
		this.ataquepower = ataquepower;
	}
	
	


}
