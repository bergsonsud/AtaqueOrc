package entidades;

public class Reino {
	

	private Titulo titulo;
	private Castelo castelo;
	private Rei rei;
	private Exercito exercito;

	public Castelo getCastelo() {
		return castelo;
	}

	public void setCastelo(Castelo castelo) {
		this.castelo = castelo;
	}

	public Rei getRei() {
		return rei;
	}

	public void setRei(Rei rei) {
		this.rei = rei;
	}

	public Exercito getExercito() {
		return exercito;
	}

	public void setExercito(Exercito exercito) {
		this.exercito = exercito;
	}


	public Titulo getTitulo() {
		return titulo;
	}

	public void setTitulo(Titulo titulo) {
		this.titulo = titulo;
	}





	

}
