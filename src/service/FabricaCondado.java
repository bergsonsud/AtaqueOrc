package service;

import entidades.Castelo;
import entidades.Exercito;
import entidades.Rei;

import entidades.Titulo;



public class FabricaCondado extends FabricaReino {
	
	
	
	
	
	@Override
	public Castelo criarCastelo() {
	Castelo castelo = new Castelo();
	castelo.setDefesa(50000);
	castelo.setAtaque(80000);
		return castelo;
	}
	
	@Override
	public Rei criarRei() {
		Rei rei = new Rei();
		rei.setNome("Frodo");	
		return rei;
	}
	
	@Override
	public Exercito criarExercito() {
		Exercito exercito = new Exercito();
		exercito.setDescricao("Exercito de Anoes");
		exercito.setQuantidade(1000);
		exercito.setAtaquepower(10*2);
		return exercito;
	}
	

	@Override
	public Titulo criarTitulo(){
		Titulo titulo = new Titulo();
		titulo.setDescricao("Condado");
		return titulo;
	}

	
	
	

}
