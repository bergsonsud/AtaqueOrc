package service;

import entidades.Castelo;
import entidades.Exercito;
import entidades.Rei;
import entidades.Titulo;

public abstract class FabricaReino {
	
	private static FabricaReino instancia;
	
	public Castelo criarCastelo(){
		return new Castelo();
		
	}

	public Rei criarRei() {
		return new Rei();
	}

	public Exercito criarExercito() {
		
		return new Exercito();
	}

	
	
	public Titulo criarTitulo(){
		return new Titulo();
	}
	
	public synchronized FabricaReino getInstance(){
		if (instancia == null) {
			/*
			System.out.println("#Singleton#");
			System.out.println("Criando instancia");
			*/
			instancia = new FabricaReino() {
			};
			
		}
		/*
		System.out.println("#Singleton#");
		System.out.println("ja instanciado");
		*/
		return instancia;
	}

}
