package service;

import entidades.Castelo;
import entidades.Exercito;
import entidades.Rei;
import entidades.Titulo;



public class FabricaMordor extends FabricaReino {
	
	
	@Override
	public Castelo criarCastelo() {
	Castelo castelo = new Castelo();
	castelo.setDefesa(80000);
	castelo.setAtaque(100000);
		return castelo;
	}
	
	@Override
	public Rei criarRei() {
		Rei rei = new Rei();
		rei.setNome("Sauron");	
		return rei;
	}
	
	@Override
	public Exercito criarExercito() {
		Exercito exercito = new Exercito();
		exercito.setDescricao("Exercito de Orcs");

		exercito.setQuantidade(1250);

		exercito.setAtaquepower(30*4);
		return exercito;
	}
	


	@Override
	public Titulo criarTitulo(){
		Titulo titulo = new Titulo();
		titulo.setDescricao("Mordor");
		return titulo;
	}

	

}
